require "../spec_helper"

describe Mongodb do
  message_length = 177
  request_id = 55
  response_to = 13
  op_code = 2001

  zero = 0
  full_collection_name = "author.book"
  flags = 0
  selector = {"first_name" => "Alexandres", "last_name" => "Dumas"} of String => Type
  update = {"first_name" => "Alexandres", "last_name" => "Dumas"} of String => Type
  documents = [selector, update]
  number_to_skip = 9
  number_to_return = 15
  query = {"first_name" => "Alexandres", "last_name" => "Dumas"} of String => Type
  return_field_selector_document = {"first_name" => "Alexandres", "last_name" => "Dumas"} of String => Type
  return_field_selector_nil = nil
  cursor_id = 13_i64
  number_of_cursor_ids = 3
  cursor_ids = [0_i64, 1_i64, 2_i64]
  message = "Some message"
  response_flags = 13
  starting_from = 2


  it "MsgHeader serialized and deserialized" do
    header = MsgHeader.new **{
      message_length: message_length,
      request_id: request_id,
      response_to: response_to,
      op_code: op_code
    }

    io = MemoryIO.new
    header.pack_to_mongo io
    io.rewind
    io.size.should eq 16

    clone = MsgHeader.unpack_from_mongo io

    clone.should eq header
  end

  it "OpUpdate serialized and deserialized" do
    header = MsgHeader.new **{
      message_length: message_length,
      request_id: request_id,
      response_to: response_to,
      op_code: op_code
    }

    op_update = OpUpdate.new **{
      header: header,
      zero: zero,
      full_collection_name: full_collection_name,
      flags: flags,
      selector: selector,
      update: update
    }

    op_update.update_header

    io = MemoryIO.new
    op_update.pack_to_mongo io
    io.rewind

    io.size.should eq op_update.header.message_length

    clone = OpUpdate.unpack_from_mongo io
    clone.should eq op_update
  end

  it "OpInsert serialized and deserialized" do
    header = MsgHeader.new **{
      message_length: message_length,
      request_id: request_id,
      response_to: response_to,
      op_code: op_code
    }

    op_insert = OpInsert.new **{
      header: header,
      flags: flags,
      full_collection_name: full_collection_name,
      documents: documents
    }

    op_insert.update_header

    io = MemoryIO.new
    op_insert.pack_to_mongo io
    io.rewind

    io.size.should eq op_insert.header.message_length

    clone = OpInsert.unpack_from_mongo io
    clone.should eq op_insert
  end

  it "OpQuery with document serialized and deserialized" do
    header = MsgHeader.new **{
      message_length: message_length,
      request_id: request_id,
      response_to: response_to,
      op_code: op_code
    }

    op_query = OpQuery.new **{
     header: header,
     flags: flags,
     full_collection_name: full_collection_name,
     number_to_skip: number_to_skip,
     number_to_return: number_to_return,
     query: query,
     return_field_selector: return_field_selector_document
    }

    op_query.update_header

    io = MemoryIO.new
    op_query.pack_to_mongo io
    io.rewind

    io.size.should eq op_query.header.message_length

    clone = OpQuery.unpack_from_mongo io
    clone.should eq op_query
  end

  it "OpQuery with nil serialized and deserialized" do
    header = MsgHeader.new **{
      message_length: message_length,
      request_id: request_id,
      response_to: response_to,
      op_code: op_code
    }

    op_query = OpQuery.new **{
      header: header,
      flags: flags,
      full_collection_name: full_collection_name,
      number_to_skip: number_to_skip,
      number_to_return: number_to_return,
      query: query,
      return_field_selector: return_field_selector_nil
    }

    op_query.update_header

    io = MemoryIO.new
    op_query.pack_to_mongo io
    io.rewind

    io.size.should eq op_query.header.message_length

    clone = OpQuery.unpack_from_mongo io
    clone.should eq op_query
  end

  it "OpGetMore serialized and deserialized" do
    header = MsgHeader.new **{
      message_length: message_length,
      request_id: request_id,
      response_to: response_to,
      op_code: op_code
    }

    op_getmore = OpGetMore.new **{
      header: header,
      zero: zero,
      full_collection_name: full_collection_name,
      number_to_return: number_to_return,
      cursor_id: cursor_id
    }

    op_getmore.update_header

    io = MemoryIO.new
    op_getmore.pack_to_mongo io
    io.rewind

    io.size.should eq op_getmore.header.message_length

    clone = OpGetMore.unpack_from_mongo io
    clone.should eq op_getmore
  end

  it "OpDelete serialized and deserialized" do
    header = MsgHeader.new **{
      message_length: message_length,
      request_id: request_id,
      response_to: response_to,
      op_code: op_code
    }

    op_delete = OpDelete.new **{
      header: header,
      zero: zero,
      full_collection_name: full_collection_name,
      flags: flags,
      selector: selector
    }

    op_delete.update_header

    io = MemoryIO.new
    op_delete.pack_to_mongo io
    io.rewind

    io.size.should eq op_delete.header.message_length

    clone = OpDelete.unpack_from_mongo io
    clone.should eq op_delete
  end

  it "OpKillCursors serialized and deserialized" do
    header = MsgHeader.new **{
      message_length: message_length,
      request_id: request_id,
      response_to: response_to,
      op_code: op_code
    }

    op_killcursors = OpKillCursors.new **{
      header: header,
      zero: zero,
      number_of_cursor_ids: number_of_cursor_ids,
      cursor_ids: cursor_ids
    }

    op_killcursors.update_header

    io = MemoryIO.new
    op_killcursors.pack_to_mongo io
    io.rewind

    io.size.should eq op_killcursors.header.message_length

    clone = OpKillCursors.unpack_from_mongo io
    clone.should eq op_killcursors
  end

  it "OpMsg serialized and deserialized" do
    header = MsgHeader.new **{
      message_length: message_length,
      request_id: request_id,
      response_to: response_to,
      op_code: op_code
    }

    op_msg = OpMsg.new **{
      header: header,
      message: message
    }

    op_msg.update_header

    io = MemoryIO.new
    op_msg.pack_to_mongo io
    io.rewind

    io.size.should eq op_msg.header.message_length

    clone = OpMsg.unpack_from_mongo io
    clone.should eq op_msg
  end

  it "OpReply serialized and deserialized" do
    header = MsgHeader.new **{
      message_length: message_length,
      request_id: request_id,
      response_to: response_to,
      op_code: op_code
    }

    op_reply = OpReply.new **{
      header: header,
      response_flags: response_flags,
      cursor_id: cursor_id,
      starting_from: starting_from,
      documents: documents
    }

    op_reply.update_header

    io = MemoryIO.new
    op_reply.pack_to_mongo io
    io.rewind

    io.size.should eq op_reply.header.message_length

    clone = OpReply.unpack_from_mongo io
    clone.should eq op_reply
  end
end
