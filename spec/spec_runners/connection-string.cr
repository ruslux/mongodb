require "file"
require "json"

struct JSON::Any
  def as_s?
    if self.class == String
      self
    else
      nil
    end
  end
end

module SpecRunner
  class Runner

  end

  class ConnectionString < Runner
    @@test_case_pathes = [
      "specifications/source/connection-string/tests/invalid-uris.json",
      "specifications/source/connection-string/tests/valid-auth.json",
      "specifications/source/connection-string/tests/valid-host_identifiers.json",
      "specifications/source/connection-string/tests/valid-options.json",
      "specifications/source/connection-string/tests/valid-unix_socket-absolute.json",
      "specifications/source/connection-string/tests/valid-unix_socket-relative.json",
      "specifications/source/connection-string/tests/valid-warnings.json"
    ]

    def self.run
      describe "connection string" do
        @@test_case_pathes.each do |test_case_path|
          json = File.read test_case_path
          self.test(json)
        end
      end
    end

    def self.test(json : String)
      test_cases = Hash(String, JSON::Any).from_json(json)["tests"]
      test_cases.each do |test_case|
        it test_case["description"].to_s do
          client = Client.new test_case["uri"].to_s

          if test_case["auth"]?.nil?
            client.password.should eq nil
            client.username.should eq nil
            client.db.should eq nil
          else
            client.password.should eq test_case["auth"]["password"].as_s?
            client.username.should eq test_case["auth"]["username"].as_s?
            client.db.should eq test_case["auth"]["db"].as_s?
          end
        end
      end
    end
  end
end
