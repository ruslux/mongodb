require "socket"
require "uri"
require "../mongodb"

struct ::Nil
  def unescape
    nil
  end

  def includes? (search : Char | String)
    false
  end
end

class ::String
  def unescape
    URI.unescape self
  end
end

module Mongodb
  module Options
    alias Type = Int32 | String | Bool | Array(Type) | Hash(Type, Type)
  end

  struct Host
    @address : String
    @port    : Int32

    def initialize(@address : String, @port : Int32)
    end
  end

  class Client
    @hosts    : Array(Host)                 = [] of Host
    @options  : Hash(String, Options::Type) = {} of String => Options::Type
    @username : String?
    @password : String?
    @db       : String?

    getter hosts
    getter username
    getter password
    getter db
    getter options

    def initialize(uri : String)
      unescaped_column = ":"

      username = nil
      password = nil
      hosts = nil

      begin
        connection_string = URI.parse(uri)

        if connection_string.scheme == "mongodb"
          userinfo = connection_string.userinfo.unescape
          username = connection_string.user.unescape
          password = connection_string.password.unescape

          if userinfo != "#{username}:#{password}"
            raise URI::Error.new "Userinfo not equal username:password"
          end

          if username.includes? unescaped_column
            raise URI::Error.new "Username has unescaped column"
          end

          if password.includes? unescaped_column
            raise URI::Error.new "Password has unescaped column"
          end

          hosts = connection_string.host.unescape
        end
      rescue ex : URI::Error
        @username = nil
        @password = nil
        @db = nil
      else
        if @username && @password && @db
          @username = username
          @password = password
          @db = db
        end
      end
    end

    def [](value : String)
      Database.new value, self
    end

    def [](value : Symbol)
      Database.new value.to_s, self
    end
  end
end
