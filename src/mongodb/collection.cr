require "../mongodb"

module Mongodb
  struct Collection
    @name : String
    @database : Database

    getter name, database

    def initialize(@name : String, @database : Database)
    end

    def insert_one(document : Document)
      documents = [] of Document
      documents << document
      query = {
        "insert" => @name,
        "documents" => [document] of Type,
        "writeConcern" => {
          "w" => 1,
          "j" => true,
          "wtimeout" => 1000
        } of String => Type,
        "ordered" => false
      } of String => Type

      request = OpQuery.new
      request.header = header_for_operation(OpCode::OP_QUERY)
      request.flags = QueryFlag::SlaveOk.value
      request.full_collection_name = "#{database.name}.$cmd"
      request.number_to_skip = 0
      request.number_to_return = 1
      request.query = query

      request.update_header

      io = MemoryIO.new
      request.pack_to_mongo io

      @database.client.requests << io
      @database.client.send
    end

    def full_collection_name
      "#{database.name}.#{@name}"
    end

    def header_for_operation(operation : OpCode)
      MsgHeader.new op_code: operation.value
    end
  end
end
