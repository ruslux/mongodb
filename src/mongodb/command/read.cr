require "../requirements"

module Mongodb
  module Crud
    module Read
      struct Collection
        # Runs an aggregation framework pipeline.
        #
        # Note: $out is a special pipeline stage that causes no results to be returned
        # from the server. As such, the iterable here would never contain documents. Drivers
        # MAY setup a cursor to be executed upon iteration against the $out collection such
        # that if a user were to iterate a pipeline including $out, results would be returned.
        #
        # @see http://docs.mongodb.org/manual/reference/command/aggregate/
        def aggregate(pipeline : Array(BSON::Document), options : AggregateOptions?) : Iterable(BSON::Document)
        end

        # Gets the number of documents matching the filter.
        #
        # @see http://docs.mongodb.org/manual/reference/command/count/
        def count(filter : BSON::Document, options : CountOptions?) : Int64
        end

        # Finds the distinct values for a specified field across a single collection.
        #
        # @see http://docs.mongodb.org/manual/reference/command/distinct/
        def distinct(field_name : String, filter : BSON::Document, options : DistinctOptions?) : Iterable(Any)
        end

        # Finds the documents matching the model.
        #
        # Note: The filter parameter below equates to the $query meta operator. It cannot
        # contain other meta operators like $maxScan. However, do not validate this document
        # as it would be impossible to be forwards and backwards compatible. Let the server
        # handle the validation.
        #
        # Note: If $explain is specified in the modifiers, the return value is a single
        # document. This could cause problems for static languages using strongly typed entities.
        #
        # @see http://docs.mongodb.org/manual/core/read-operations-introduction/
        def find(filter : BSON::Document, options : FindOptions?) : Iterable(BSON::Document)
        end
      end

      struct AggregateOptions
        # Enables writing to temporary files. When set to true, aggregation stages
        # can write data to the _tmp subdirectory in the dbPath directory.
        # The default is no value: the driver sends no "allowDiskUse" option to the
        # server with the "aggregate" command.
        #
        # @see http://docs.mongodb.org/manual/reference/command/aggregate/
        @allow_disk_use : Bool?

        # The number of documents to return per batch.
        #
        # For servers < 2.6, this option is ignored as aggregation cursors are not available.
        # The default is no value: the driver sends no "batchSize" option to the server with
        # the "aggregate" command, thus accepting the server default batch size.
        #
        # @see http://docs.mongodb.org/manual/reference/command/aggregate/
        @batch_size : Int32?

        # If true, allows the write to opt-out of document level validation. This only applies
        # when the $out stage is specified.
        #
        # On servers >= 3.2, the default is no value: no
        # "bypassDocumentValidation" option is sent with the "aggregate" command.
        #
        # On servers < 3.2, this option is ignored.
        @bypass_document_validation : Bool?

        # The maximum amount of time to allow the query to run.
        # The default is no value: the driver sends no "maxTimeMS" option to the
        # server with the "aggregate" command.
        #
        # @see http://docs.mongodb.org/manual/reference/command/aggregate/
        @max_time_ms : Int64?

        # Indicates whether the command will request that the server provide results using a cursor.
        #
        # For servers < 2.6, this option is ignored as aggregation cursors are not available.
        # For servers >= 2.6, this option allows users to turn off cursors if necessary to aid in mongod/mongos upgrades.
        # The default value is true: the driver sends "cursor: {}" to the server with the "aggregate" command
        # by default.
        #
        # @see http://docs.mongodb.org/manual/reference/command/aggregate/
        @use_cursor : Bool?
      end

      struct CountOptions
        # The index to use. The default is no hint.
        #
        # @see http://docs.mongodb.org/manual/reference/command/count/
        @hint : String | BSON::Document | Nil

        # The maximum number of documents to count. The default is no limit.
        #
        # @see http://docs.mongodb.org/manual/reference/command/count/
        @limit : Int64?

        # The maximum amount of time to allow the query to run.
        # The default is no maxTimeMS.
        #
        # @see http://docs.mongodb.org/manual/reference/command/count/
        @max_time_ms : Int64?

        # The number of documents to skip before counting. The default is no skip.
        #
        # @see http://docs.mongodb.org/manual/reference/command/count/
        @skip : Int64?
      end

      struct DistinctOptions
        # The maximum amount of time to allow the query to run.
        # The default is no maxTimeMS.
        #
        # @see http://docs.mongodb.org/manual/reference/command/distinct/
        @max_time_ms : Int64?
      end

      enum CursorType
        # The default value. A vast majority of cursors will be of this type.
        NON_TAILABLE
        # Tailable means the cursor is not closed when the last data is retrieved.
        # Rather, the cursor marks the final object’s position. You can resume
        # using the cursor later, from where it was located, if more data were
        # received. Like any “latent cursor”, the cursor may become invalid at
        # some point (CursorNotFound) – for example if the final object it
        # references were deleted.
        #
        # @see http://docs.mongodb.org/meta-driver/latest/legacy/mongodb-wire-protocol/#op-query
        TAILABLE
        # Combines the tailable option with awaitData, as defined below.
        #
        # Use with TailableCursor. If we are at the end of the data, block for a
        # while rather than returning no data. After a timeout period, we do return
        # as normal. The default is true.
        #
        # @see http://docs.mongodb.org/meta-driver/latest/legacy/mongodb-wire-protocol/#op-query
        TAILABLE_AWAIT
      end

      struct FindOptions
        # Get partial results from a mongos if some shards are down (instead of throwing an error).
        #
        # The default in servers >= 3.2 is no value: no "allowPartialResults" option is sent with
        # the "find" command.
        #
        # In servers < 3.2, the Partial OP_QUERY flag defaults to false.
        #
        # @see http://docs.mongodb.org/meta-driver/latest/legacy/mongodb-wire-protocol/#op-query
        @allow_partial_results : Bool?

        # The number of documents to return per batch.
        #
        # In servers < 3.2, this is combined with limit to create the OP_QUERY numberToReturn value.
        #
        # The default is no value: the driver accepts the server default batch size.
        #
        # @see http://docs.mongodb.org/manual/reference/method/cursor.batchSize/
        @batch_size : Int32?

        # Attaches a comment to the query. If $comment also exists
        # in the modifiers document, the comment field overwrites $comment.
        # The default is no comment.
        #
        # @see http://docs.mongodb.org/manual/reference/operator/meta/comment/
        @comment : String?

        # Indicates the type of cursor to use. This value includes both
        # the tailable and awaitData options.
        #
        # The default in servers >= 3.2 is no value: no "awaitData" or "tailable"
        # option is sent with the "find" command.
        #
        # In servers < 3.2, the AwaitData OP_QUERY flag and the Tailable OP_QUERY
        # flag default to false.
        #
        # @see http://docs.mongodb.org/meta-driver/latest/legacy/mongodb-wire-protocol/#op-query
        @cursor_type : CursorType?

        # The maximum number of documents to return.
        #
        # In servers < 3.2, this is combined with batchSize to create the OP_QUERY numberToReturn value.
        #
        # The default is no limit.
        #
        # @see http://docs.mongodb.org/manual/reference/method/cursor.limit/
        @limit : Int32?

        # The maximum amount of time for the server to wait on new documents to satisfy a tailable cursor
        # query. This only applies to a TAILABLE_AWAIT cursor. When the cursor is not a TAILABLE_AWAIT cursor,
        # this option is ignored.
        #
        # On servers >= 3.2, this option will be specified on the getMore command as "maxTimeMS". The default
        # is no value: no "maxTimeMS" is sent to the server with the getMore command.
        #
        # On servers < 3.2, this option is ignored.
        @max_await_time_ms : Int64?

        # The maximum amount of time to allow the query to run. If $maxTimeMS also exists
        # in the modifiers document, the maxTimeMS field overwrites $maxTimeMS.
        #
        # The default is no maxTimeMS.
        #
        # @see http://docs.mongodb.org/manual/reference/operator/meta/maxTimeMS/
        @max_time_ms : Int64?

        # Meta-operators modifying the output or behavior of a query.
        # The default is no modifers.
        #
        # @see http://docs.mongodb.org/manual/reference/operator/query-modifier/
        @modifiers : BSON::Document?

        # The server normally times out idle cursors after an inactivity period (10 minutes)
        # to prevent excess memory use. Set this option to prevent that.
        #
        # The default in servers >= 3.2 is no value: no "noCursorTimeout" option is sent with
        # the "find" command.
        #
        # In servers < 3.2, the NoCursorTimeout OP_QUERY flag defaults to false.
        #
        # @see http://docs.mongodb.org/meta-driver/latest/legacy/mongodb-wire-protocol/#op-query
        @no_cursor_timeout : Bool?

        # Internal replication use only - driver should not set
        #
        # The default in servers >= 3.2 is no value: no "oplogReplay" option is sent with
        # the "find" command.
        #
        # In servers < 3.2, the OplogReplay OP_QUERY flag defaults to false.
        #
        # @see http://docs.mongodb.org/meta-driver/latest/legacy/mongodb-wire-protocol/#op-query
        @oplog_replay : Bool?

        # Limits the fields to return for all matching documents.
        # The default is no projection.
        #
        # @see http://docs.mongodb.org/manual/tutorial/project-fields-from-query-results/
        @projection : BSON::Document?

        # The number of documents to skip before returning.
        #
        # In servers < 3.2, this is a wire protocol parameter that defaults to 0.
        #
        # The default in servers >= 3.2 is no skip: no "skip" option is sent with
        # the "find" command.
        #
        # @see http://docs.mongodb.org/manual/reference/method/cursor.skip/
        @skip : Int32?

        # The order in which to return matching documents. If $orderby also exists
        # in the modifiers document, the sort field overwrites $orderby.
        # The default is no sort.
        #
        # @see http://docs.mongodb.org/manual/reference/method/cursor.sort/
        @sort : BSON::Document?
      end
    end
  end
end
