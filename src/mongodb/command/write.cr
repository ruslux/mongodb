require "../requirements"

module Mongodb
  module Crud
    module Write
      struct Collection
        # Sends a batch of writes to the server at the same time.
        #
        # NOTE: see the FAQ about the previous bulk API and how it relates to this.
        # @see http://docs.mongodb.org/manual/reference/command/delete/
        # @see http://docs.mongodb.org/manual/reference/command/insert/
        # @see http://docs.mongodb.org/manual/reference/command/update/
        # @throws BulkWriteException
        def bulk_write(requests : Array(WriteModel), options : BulkWriteOptions?) : BulkWriteResult
        end

        # Inserts the provided document. If the document is missing an identifier,
        # the driver should generate one.
        #
        # @see http://docs.mongodb.org/manual/reference/command/insert/
        # @throws WriteException
        def insert_one(document : Document, options : InsertOneOptions?) : InsertOneResult
        end

        # Inserts the provided documents. If any documents are missing an identifier,
        # the driver should generate them.
        #
        # Note that this uses the bulk insert command underneath and should not
        # use OP_INSERT. This will be slow on < 2.6 servers, so document
        # your driver appropriately.
        #
        # @see http://docs.mongodb.org/manual/reference/command/insert/
        # @throws BulkWriteException
        def insert_many(documents : Iterable(Document), options : InsertManyOptions?) : InsertManyResult
        end

         # Deletes one document.
         #
         # @see http://docs.mongodb.org/manual/reference/command/delete/
         # @throws WriteException
        def delete_one(filter : Document) : DeleteResult
        end

        # Deletes multiple documents.
        #
        # @see http://docs.mongodb.org/manual/reference/command/delete/
        # @throws WriteException
        def delete_many(filter : Document) : DeleteResult
        end

        # Replaces a single document.
        #
        # @see http://docs.mongodb.org/manual/reference/command/update/
        # @throws WriteException
        def replace_one(filter : Document, replacement : Document, options : UpdateOptions?) : UpdateResult
        end

        # Updates one document.
        #
        # @see http://docs.mongodb.org/manual/reference/command/update/
        # @throws WriteException
        def update_one(filter : Document, update : Document, options : UpdateOptions?) : UpdateResult
        end

        # Updates multiple documents.
        #
        # @see http://docs.mongodb.org/manual/reference/command/update/
        # @throws WriteException
        def update_many(filter : Document, update : Document, options : UpdateOptions?) : UpdateResult
        end
      end

      struct BulkWriteOptions
        # If true, when a write fails, return without performing the remaining
        # writes. If false, when a write fails, continue with the remaining writes, if any.
        # Defaults to true.
        @ordered                    : Bool = true

        # If true, allows the write to opt-out of document level validation.
        #
        # On servers >= 3.2, the default is no value: no
        # "bypassDocumentValidation" option is sent with the write command.
        #
        # On servers < 3.2, this option is ignored.
        @bypass_document_validation : Bool?
      end

      struct InsertOneOptions
        # If true, allows the write to opt-out of document level validation.
        #
        # On servers >= 3.2, the default is no value: no
        # "bypassDocumentValidation" option is sent with the "insert" command.
        #
        # On servers < 3.2, this option is ignored.
        @bypass_document_validation : Bool?
      end

      struct InsertManyOptions
        # If true, allows the write to opt-out of document level validation.
        #
        # On servers >= 3.2, the default is no value: no
        # "bypassDocumentValidation" option is sent with the "insert" command.
        #
        # On servers < 3.2, this option is ignored.
        @bypass_document_validation : Bool?

        # If true, when an insert fails, return without performing the remaining
        # writes. If false, when a write fails, continue with the remaining writes, if any.
        # Defaults to true.
        @ordered                    : Bool = true
      end

      struct UpdateOptions
        # If true, allows the write to opt-out of document level validation.
        #
        # On servers >= 3.2, the default is no value: no
        # "bypassDocumentValidation" option is sent with the "update" command.
        #
        # On servers < 3.2, this option is ignored.
        @bypass_document_validation : Bool?

        # When true, creates a new document if no document matches the query. The default is false.
        #
        # @see http://docs.mongodb.org/manual/reference/command/update/
        @upsert                     : Bool?
      end
    end

    module WriteModel
      # marker interface for writes that can be batched together.
      abstract struct WriteModel
      end

      struct InsertOneModel < WriteModel
        # The document to insert.
        #
        # @see http://docs.mongodb.org/manual/reference/command/insert/
        @document : BSON::Document = BSON::Document.new
      end

      struct DeleteOneModel < WriteModel
        # The filter to limit the deleted documents.
        #
        # @see http://docs.mongodb.org/manual/reference/command/delete/
        @filter : BSON::Document = BSON::Document.new
      end

      struct DeleteManyModel < WriteModel
        # The filter to limit the deleted documents.
        #
        # @see http://docs.mongodb.org/manual/reference/command/delete/
        @filter : BSON::Document = BSON::Document.new
      end

      struct ReplaceOneModel < WriteModel
        # The filter to limit the replaced document.
        #
        # @see http://docs.mongodb.org/manual/reference/command/update/
        @filter      : BSON::Document = BSON::Document.new

        # The document with which to replace the matched document.
        #
        # @see http://docs.mongodb.org/manual/reference/command/update/
        @replacement : BSON::Document = BSON::Document.new

        # When true, creates a new document if no document matches the query. The default is false.
        #
        # @see http://docs.mongodb.org/manual/reference/command/update/
        @upsert      : Bool?          = false
      end

      struct UpdateOneModel < WriteModel
        # The filter to limit the updated documents.
        #
        # @see http://docs.mongodb.org/manual/reference/command/update/
        @filter : BSON::Document = BSON::Document.new

        # A document containing update operators.
        #
        # @see http://docs.mongodb.org/manual/reference/command/update/
        @update : BSON::Document = BSON::Document.new

        # When true, creates a new document if no document matches the query. The default is false.
        #
        # @see http://docs.mongodb.org/manual/reference/command/update/
        @upsert : Bool?          = false
      end

      struct UpdateManyModel < WriteModel
        # The filter to limit the updated documents.
        #
        # @see http://docs.mongodb.org/manual/reference/command/update/
        @filter : BSON::Document = BSON::Document.new

        # A document containing update operators.
        #
        # @see http://docs.mongodb.org/manual/reference/command/update/
        @update : BSON::Document = BSON::Document.new

        # When true, creates a new document if no document matches the query. The default is false.
        #
        # @see http://docs.mongodb.org/manual/reference/command/update/
        @upsert : Bool?          = false
      end
    end

    module WriteResult
      struct BulkWriteResult(T)
        # Indicates whether this write result was ackowledged. If not, then all
        # other members of this result will be undefined.
        #
        # NOT REQUIRED: Drivers may choose to not provide this property.
        @acknowledged   : Boolean        = false

        # Number of documents inserted.
        @inserted_count : Int64          = 0_i64

        # Map of the index of the operation to the id of the inserted document.
        #
        # NOT REQUIRED: Drivers may choose to not provide this property.
        @inserted_ids   : Hash(Int64, T) = Hash(Int64, T).new

        # Number of documents matched for update.
        @matched_count  : Int64          = 0_i64

        # Number of documents modified.
        @modified_count : Int64          = 0_i64

        # Number of documents deleted.
        @deleted_count  : Int64          = 0_i64

        # Number of documents upserted.
        @upserted_count : Int64          = 0_i64

        # Map of the index of the operation to the id of the upserted document.
        @upserted_ids   : Hash(Int64, T) = Hash(Int64, T).new
      end

      struct InsertOneResult(T)
        # Indicates whether this write result was ackowledged. If not, then all
        # other members of this result will be undefined.
        #
        # NOT REQUIRED: Drivers may choose to not provide this property.
        @acknowledged : Bool = false

        # The identifier that was inserted. If the server generated the identifier, this value
        # will be null as the driver does not have access to that data.
        #
        # NOT REQUIRED: Drivers may choose to not provide this property.
        @inserted_id  : T    = T.new
      end

      struct InsertManyResult(T)
        # Indicates whether this write result was ackowledged. If not, then all
        # other members of this result will be undefined.
        #
        # NOT REQUIRED: Drivers may choose to not provide this property.
        @acknowledged : Bool           = false

        # Map of the index of the inserted document to the id of the inserted document.
        #
        # NOT REQUIRED: Drivers may choose to not provide this property.
        @inserted_ids : Hash(Int64, T) = Hash(Int64, T).new
      end

      struct DeleteResult
        # Indicates whether this write result was ackowledged. If not, then all
        # other members of this result will be undefined.
        #
        # NOT REQUIRED: Drivers may choose to not provide this property.
        @acknowledged  : Bool   = false

        # The number of documents that were deleted.
        @deleted_count : Int64 = 0_i64
      end

      struct UpdateResult(T)
        # Indicates whether this write result was ackowledged. If not, then all
        # other members of this result will be undefined.
        #
        # NOT REQUIRED: Drivers may choose to not provide this property.
        @acknowledged   : Bool  = false

        # The number of documents that matched the filter.
        @matched_count  : Int64 = 0_i64

        # The number of documents that were modified.
        @modified_count : Int64 = 0_i64

        # The identifier of the inserted document if an upsert took place.
        @upserted_id    : T     = T.new
      end
    end
  end
end
