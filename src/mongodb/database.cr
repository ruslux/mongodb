require "../mongodb"

module Mongodb
  class Database
    @name : String
    @client : Client

    getter name
    getter client

    def initialize(@name : String, @client : Client)
    end

    def [](value : String)
      Collection.new value, self
    end

    def [](value : Symbol)
      Collection.new value.to_s, self
    end
  end
end
