require "./requirements"
require "io/memory_io"

include BSON

struct Int32 < Int
  def pack_to_mongo(io : IO)
    to_io io, IO::ByteFormat::LittleEndian
  end

  def self.unpack_from_mongo(io : IO)
    io.read_bytes(Int32, IO::ByteFormat::LittleEndian)
  end

  def octet_size
    4
  end
end

struct Int64 < Int
  def pack_to_mongo(io : IO)
    to_io io, IO::ByteFormat::LittleEndian
  end

  def self.unpack_from_mongo(io : IO)
    io.read_bytes(Int64, IO::ByteFormat::LittleEndian)
  end

  def octet_size
    8
  end
end


class String
  def pack_to_mongo(io : IO)
    to_bson_cstring io
  end

  def self.unpack_from_mongo(io : IO)
    from_bson_cstring io
  end

  def octet_size
    bytesize + 1
  end
end

struct Nil
  def pack_to_mongo(io : IO)
  end

  def self.unpack_from_mongo(io : IO)
    begin
      Document.unpack_from_mongo io
    rescue IO::EOFError
      nil
    end
  end

  def octet_size
    0
  end
end

class Hash(K, V)
  def pack_to_mongo(io : IO)
    to_bson io
  end

  def self.unpack_from_mongo(io : IO) : Hash(K, V)
    Hash(K, V).from_bson io
  end

  def octet_size
    bson_size
  end
end

class Array(T)
  def pack_to_mongo(io : IO)
    each do |element|
      element.pack_to_mongo io
    end
  end

  def self.unpack_from_mongo(io : IO) : Array(T)
    items = [] of T

    loop do
      begin
        item = T.from_bson io
      rescue IO::EOFError
         break
      end
      items << item
    end

    items
  end

  def octet_size
    size = 0
    each do |element|
      size += element.octet_size
    end
    size
  end
end

module Mongodb
    Int32_size = 4
    Int64_size = 8

    @[Flags]
    enum UpdateFlag
      Upsert      =  0b1
      MultiUpdate = 0b10
    end

    @[Flags]
    enum InsertFlag
      ContinueOnError = 0b1
    end

    @[Flags]
    enum QueryFlag
      Reserved        =        0b1
      TailableCursor  =       0b10
      SlaveOk         =      0b100
      OplogReplay     =     0b1000
      NoCursorTimeout =    0b10000
      AwaitData       =   0b100000
      Exhaust         =  0b1000000
      Partial         = 0b10000000
    end

    @[Flags]
    enum DeleteFlag
      SingleRemove = 0b1
    end

    @[Flags]
    enum ResponseFlag
      CursorNotFound   =    0b1
      QueryFailure     =   0b10
      ShardConfigState =  0b100
      AwaitCapable     = 0b1000
    end

    enum OpCode
      OP_REPLY        = 1
      OP_MSG          = 1000
      OP_UPDATE       = 2001
      OP_INSERT       = 2002
      RESERVED        = 2003
      OP_QUERY        = 2004
      OP_GET_MORE     = 2005
      OP_DELETE       = 2006
      OP_KILL_CURSORS = 2007
    end

  TYPES = {
    :header                => MsgHeader,
    :zero                  => Int32,
    :full_collection_name  => String,
    :flags                 => Int32,
    :selector              => Document,
    :update                => Document,
    :documents             => Array(Document),
    :number_to_skip        => Int32,
    :number_to_return      => Int32,
    :query                 => Document,
    :return_field_selector => Document | Nil,
    :cursor_id             => Int64,
    :number_of_cursor_ids  => Int32,
    :cursor_ids            => Array(Int64),
    :response_flags        => Int32,
    :starting_from         => Int32,
    :message               => String,
    :message_length        => Int32,
    :request_id            => Int32,
    :response_to           => Int32,
    :op_code               => Int32
  }

  DEFAULTS = {
    Int32           => 0,
    Int64           => 0_i64,
    String          => "",
    MsgHeader       => MsgHeader.new,
    Document        => Document.new,
    Array(Document) => ([] of Document),
    Array(Int64)    => ([] of Int64),
    Document | Nil  => nil
  }

  UNPACK = {
    Int32           => Int32,
    Int64           => Int64,
    String          => String,
    MsgHeader       => MsgHeader,
    Document        => Document,
    Array(Document) => Array(Document),
    Array(Int64)    => Array(Int64),
    Document | Nil  => Nil
  }
  module UpdateHeader
    def update_header
      @header.message_length = octet_size
    end
  end

  module MacroProtocol
    macro protocol(fields)
      {% for name, index in fields %}
    	@{{name.id}} : {{TYPES[name]}}

      def {{name.id}}
        @{{name.id}}
      end

      def {{name.id}}=(@{{name.id}} : {{TYPES[name]}})
      end
      {% end %}

      def initialize(
        {% for name, index in fields %}
      	@{{name.id}} : {{TYPES[name]}} = {{DEFAULTS[TYPES[name]]}},
        {% end %}
      )
      end

      def pack_to_mongo(io : IO)
        {% for name, index in fields %}
        @{{name.id}}.pack_to_mongo(io)
        {% end %}
      end

      def self.unpack_from_mongo(io : IO)
        instance = self.new
        {% for name, index in fields %}
        instance.{{name.id}} = {{UNPACK[TYPES[name]]}}.unpack_from_mongo(io)
        {% end %}
        instance
      end

      def octet_size
        size = 0
        {% for name, index in fields %}
        size += @{{name.id}}.octet_size
        {% end %}
        size
      end
    end
  end

  struct MsgHeader
    include MacroProtocol
    protocol [:message_length, :request_id, :response_to, :op_code]
  end

  struct OpUpdate
    include MacroProtocol
    protocol [:header, :zero, :full_collection_name, :flags, :selector, :update]
    include UpdateHeader
  end

  struct OpInsert
    include MacroProtocol
    protocol [:header, :flags, :full_collection_name, :documents]
    include UpdateHeader
  end

  struct OpQuery
    include MacroProtocol
    protocol [:header, :flags, :full_collection_name, :number_to_skip, :number_to_return, :query, :return_field_selector]
    include UpdateHeader
  end

  struct OpGetMore
    include MacroProtocol
    protocol [:header, :zero, :full_collection_name, :number_to_return, :cursor_id]
    include UpdateHeader
  end

  struct OpDelete
    include MacroProtocol
    protocol [:header, :zero, :full_collection_name, :flags, :selector]
    include UpdateHeader
  end

  struct OpKillCursors
    include MacroProtocol
    protocol [:header, :zero, :number_of_cursor_ids, :cursor_ids]
    include UpdateHeader
  end

  struct OpMsg
    include MacroProtocol
    protocol [:header, :message]
    include UpdateHeader
  end

  struct OpReply
    include MacroProtocol
    protocol [:header, :response_flags, :cursor_id, :starting_from, :documents]
    include UpdateHeader
  end
end
